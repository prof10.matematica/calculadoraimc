package com.example.calculadoraimc;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private EditText editTextPeso;
    private EditText editTextAltura;
    private TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextPeso = findViewById(R.id.editTextPeso);
        editTextAltura = findViewById(R.id.editTextAltura);
        textViewResultado = findViewById(R.id.textViewResultado);

    }
    public void calcularImc(View v){
        String pesoString = editTextPeso.getText().toString();
        String alturaString = editTextAltura.getText().toString();

        if(!pesoString.isEmpty() && !alturaString.isEmpty()){

            float peso = Float.parseFloat(pesoString);
            float altura = Float.parseFloat(alturaString);

            float imc = peso/(altura * altura);

            DecimalFormat df = new DecimalFormat("#.##");
            String imcFormatado = df.format(imc);
            String condicao = "";

            if(imc < 18.5){
                condicao = "MAGREZA";
            } else if (imc > 18.5 && imc < 24.9) {
                condicao = "NORMAL";
            } else if (imc > 24.9 && imc < 30) {
                condicao = "SOBREPESO";
            } else {
                condicao = "OBESIDADE";
            }

            textViewResultado.setText("Seu IMC é: " + imcFormatado + " kg/m². Sua condição é: " + condicao);

        }else{

            Toast.makeText(getApplicationContext(), "Preencha peso e altura", Toast.LENGTH_SHORT).show();

        }

    }

    public void limparCampos(View v){
        editTextPeso.setText("");
        editTextAltura.setText("");
        textViewResultado.setText("");
    }
}